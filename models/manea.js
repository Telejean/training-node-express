module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "manea",
    {
      denumire: { type: DataTypes.STRING, allowNull: false },
      artist: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      // freezeTableName: true,
      tableName: "manele",
    }
  );
};
